module.exports = (async () => {
  return {
    assets: ['./src/assets/fonts/'],
    dependencies: {
      'react-native-video': {
        platforms: {
          android: {
            // This is required to ensure Android uses Exoplayer when using RN Autolinking. See: https://github.com/react-native-community/react-native-video/issues/1746#issuecomment-535310510
            sourceDir: '../node_modules/react-native-video/android-exoplayer',
          },
        },
      },
    },
  };
})();
