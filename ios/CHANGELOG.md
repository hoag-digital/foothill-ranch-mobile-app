# 1.20.3 iOS Beta (2020-03-06)

### Bug fixes
- **reservation:** fixing data error on reservation confirmations screen ([3ddc5453](/3ddc54533e5aa137feea3e778593f36e2ebf8e67))

### Building system
- **changelog:** Update Android CHANGELOG [skip ci] ([105044cb](/105044cbf95293797beb6412bf23f37ec597f6cc))

### Other work
- RC 3/6/2020 Merge branch 'master' into beta ([a2479619](/a247961950351383805a2e7801a4767576f0212a))
- trigger iOS ([d3a85f81](/d3a85f8157f1e199a1875d494edced91e186241f))

# 1.20.2 iOS Beta (2020-03-06)

### Bug fixes
- care screen loading ([5df0958f](/5df0958f28d7bfa21802641687f3ad0d5f2d8ef8))

### Building system
- **changelog:** Update Android CHANGELOG [skip ci] ([5e199b0b](/5e199b0b5c6e24680dc6395325ae720282238514))

### Other work
- An/fix urgent care ([dc028de9](/dc028de9e696e7330bfcbdcc2ac13da431a1d5b8))
- Release candidate 3/5/2020 ([778eb0ba](/778eb0ba7f16fbbf5c1bb5022035f95f6dde76df))
- trigger for iOS ([0280c69d](/0280c69d5a54b8ae9323bbdbdb1471000d3bab27))
- trigger ios ([b0620887](/b0620887a8728a546489ac31bb124acc6518598a))
- force iOS build ([31992cdb](/31992cdba300505f12ffe0f5cf217cb2f0f1b82e))

# 1.20.1 iOS Beta (2020-03-06)

### Features
- passes user context to sentry when logged in ([6d59ef61](/6d59ef616aceab165fe4ae597686a0cb751a05a1))
- **navigation_redirect:** add the ability to redirect from auth by specifying a navigation action and an optional callback on success as a navigation param. ([463733f4](/463733f448b2d18d3453cd5ba9cc62dc8efbac8d))
- **reservation_modal:** parameterize the ReservationConfirmation screen to be extendable ([a6a85154](/a6a8515430f836513badbf067f9bad8247c97c38))

### Bug fixes
- **Icons:** As a developer, I should investigate why icons are not displaying on Android ([0e66f8b8](/0e66f8b81ed7af56c2a6e9d6c33a3d6d5c47a1ac))
- **tab-navigation:** Reset navigation state when switching tabs after confirming a class scheduling. ([277f1909](/277f1909e86d12ddc2fa2fa7c50a5b84caa22b97))
- Beta Release Update ([8c9e754c](/8c9e754c9b9dc55d4e32ae9ef0f89e998bad5d7e))

### Building system
- **posts:** format news content with updated backend util ([a3c8e24d](/a3c8e24d057f5147a017c0041b6d754df33e8ca7))
- **mysql migration:** frontend updates to facilitate the modified backend schema ([8c9dde71](/8c9dde719e81f698b62c2b66c9bbfdff54662ddf))
- **changelog:** Update Android CHANGELOG [skip ci] ([d08db38d](/d08db38d0f1adc9644394de5a8cfdb7d14d7a375))

### Other work
- feat(video_controls): ([11f1efd8](/11f1efd89c6dd1dbaa69a2bf0a49b2abbbe83a65))
- fix time format in schedule screen ([b228ef87](/b228ef87c18336d72c585ea3aa36e0f9a1f8a436))
- release beta ([756fc7f5](/756fc7f55321ef569b5ed97ead18c4560299bc37))
- Trigger build for iOSn ([56681f60](/56681f6021ab9a3f7d302347991ea9e80fa8c6ac))

# 1.17.6 iOS Beta (2020-03-04)

### Features
- **Navigation:** updated hamburger menu and auth modal behaviors ([a9e14ffb](/a9e14ffb1199467e982d9fbb41bb3f770f93e248))
- **Program:** Does not allow reservation of program that occurs after token expires ([985b71f9](/985b71f94c61b5b34f0fbc63f4c18d589df9c2f6))
- add womens link to physicians screen ([97e5e5a6](/97e5e5a6958de9b447cc9db011e28cff382f18eb))
- **events:** Updating Post and Class Description Parsing ([f4d3f5e0](/f4d3f5e09b45dfec72745be2280b651838aab4b8))
- Display Error Modal when class is at capacity ([8f22c437](/8f22c437ce727fbfdc420531c0765b9700401e16))

### Bug fixes
- Keyboard Dismiss on Registration when moving fields ([dfd8a11e](/dfd8a11ea506c0cbf54050b93468db93ec5b5173))
- Login Modal Causing Flickering and refreshing of screens behind modal ([f3701b15](/f3701b153f5d9083553479049c207eb370979d05))
- fixes closing nav on outside press (android) ([27eb7bc7](/27eb7bc75336a934fac75fd94a834f8d2f92ac89))
- update cache policy for me query ([5644abf5](/5644abf54d1f8eb710b1ab7277d59571cc7a03f0))
- stop the video and reset when navigating ([f6910fe3](/f6910fe3e3e9250ed06d0e1c1a6c9e81bf409164))
- fixes text cut off on class reservation ([78be182f](/78be182f0e568adcd1de08b53c635c15410fc96b))
- **shcedule screen:** fixing shedule screen and post formatting ([d2b855e8](/d2b855e8b09e15761951a773f722e878bc2b40c9))
- refetch me when redeeming token to ensure we set expiration properly ([6f3df875](/6f3df875a7a2ba2866915cf662335a114f4ae587))

### Building system
- **test:** fix TS definition so test passes ([fe3328b0](/fe3328b061fcd3348ab239e7d9848e9330eb1017))
- **images:** check image size and minify on commit ([d34c463d](/d34c463df2aacd9bf555d59fbfd26a07581cf872))
- **build:** Fix linking warnings ([642411e0](/642411e0e3c7ad6e01d8cfc80572bb5f12cba7d2))
- **ci:** Pull before committing changelog, ensure we install libjpeg ([6ff4af21](/6ff4af21213f96b6d3c14cfcc00074254393dfaf))
- update copy for login modal ([1015c598](/1015c598237a4a9d878c6afd2492709e9a42c252))
- update copy on schedule screen ([e38d65da](/e38d65da4bc2e9aa1e75b5e3170f3c0193e3df39))
- **changelog:** Update Android CHANGELOG [skip ci] ([7ea46491](/7ea4649122424902a6b60794c366472dc3ebc77d))

### Other work
- remove git_pull for release temp fix ([19c8653a](/19c8653afc1eddf5ae6eab59732cd9176dfdce58))
- Trigger build ([cf584ebb](/cf584ebb2b47eac04dd15ae159bf2b76bfae811c))
- trigger ios to build again ([b4b4f41f](/b4b4f41fc3f91eff231ae7e903fe877cd764e6da))

# 1.12.1 iOS Beta (2020-02-26)

### Features
- **Schedule:** Cancel Events UI and Mutation ([32d358b8](/32d358b8cfdf8c54f1437c0c556bec21cf560f71))
- **nav-drawer:** updates the drawer to use an overlay with semi transparency ([49f35f6c](/49f35f6c0b329b38c4e7d0000a898f4b88c049a6))
- **Class Reservations:** Wires up class/biocircuit reservations and adds additional UI states ([c8dd98f4](/c8dd98f4ebcc01c1945a4b1b748d3b2467678629))
- **nav:** adds componentDidUpdate to keep animtedtabbar in sync with nav state ([f3050955](/f30509552a012e71ebfc5d483daa13f0b6498526))

### Bug fixes
- **ios:** Avoid a crash when using swipe gestures to navigate ([6ce08fd0](/6ce08fd0d72af659ea01530469350e33a2d62598))
- **login:** Add Keyboard Avoiding View to login ([5a6377b6](/5a6377b6918f2966297cc70cdbff7dbd84cb846e))
- display program or class on reservation modal depending on type ([9ffc3377](/9ffc3377de8082acda337810febca3f5cce4d314))
- login text and placeholder colors ([51dc7dd1](/51dc7dd14df34a51f0298e2c9f48d01fe7315181))
- **classes:** class name length on reservation modal ([f46cc83b](/f46cc83b97b56cd181ec3066fa42e4b64bdfc1d2))
- status bar display with full-bleed images ([490eef34](/490eef3416361efd6291b3becf3787d8890c617e))
- **nav-icons:** fix login and logout icon ([10a12cac](/10a12cac109943843ecfcecaa7117706bc353220))
- **news:** add generic news image for fallback ([f29aa539](/f29aa53979adcd1bb39a5c1b610459a28857e90e))
- **touchables:** Add Default hitslop to touchables ([6b0fde3a](/6b0fde3ae3347af098db61d71ee8cda6134f95b0))
- navigation from schedule to program ([c82fa0d0](/c82fa0d04275cf5fdc804997e7855212fa663ee2))
- Generic Image Fallback for HomeScreen ([a5901fef](/a5901fefe9980bab9e526aba7a5ad80f709202c7))

### Building system
- updates floating button default text color ([8b6bfe87](/8b6bfe87d6023b1f15c642fd83d1cfe2b12adfb3))
- **changelog:** Update Android CHANGELOG [skip ci] ([feaca5f6](/feaca5f67e165a7af010da9cf54049f3d06a71fa))
- Update README to trigger iOS build ([dc7bfc34](/dc7bfc3486d151cc01fdab18846c82a8cb8c1d77))

### Other work
- fix:news fallback display conditionally ([22b5d2b5](/22b5d2b502cda65645553f5111ad8ac1e14d1f54))

# 1.8.4 iOS Beta (2020-02-23)

### Features
- **images:** adds fallback images for missing api assets ([cc62c5f9](/cc62c5f984040a84347c7b76ef91dedd5ab05672))
- **Navigation:** Updates to the Custom Drawer ([cc142af1](/cc142af15f4922a1162c44c5a78c59861b997beb))
- **Registration:** Implement Registration & Login Error Handling ([09a0ec17](/09a0ec1712a698625c6c9685d25de9e7a8738ed0))
- **Programs:** Program Class List & Reservation Confirmation ([45a1e841](/45a1e841b41bd67489a0ecdaf147c34e1295d512))
- **appointments:** allow users to schedule a FR appointment ([cae086a5](/cae086a51dd952b831cff9c75fd9e1deb4d7776c))
- **schedule:** migrating schedule screen off of mock data ([d12deab5](/d12deab577b0b9db856e0c2b6cad36ad9b2f1c61))
- **login:** adds back button to login ([f26016df](/f26016df7a2c3596ce15e14e816dd0b911d3b4bc))
- **register:** add better validation, first name and last name fields ([0d232ce8](/0d232ce8ee370f187046ef0c97f15a3f4ced1e86))

### Bug fixes
- **fastlane:** fix alpha lane ([96673af8](/96673af8058267d389685b1e20414f484ab2f780))
- **class-details:** enables Reserve buttons conditionally based on number of spaces remaining ([9b74dcd5](/9b74dcd5d41d1e3c26cfd72ac880c1ef84bd20a2))
- **beta:** Fix default provisioning and commit path in beta lane ([f8da8a29](/f8da8a29086ff3af1ecbc28c9b1c100134342d17))
- **login:** show error if invalid credentials passed ([dc99be89](/dc99be896c545390dfb5229da3365b2252c41e45))
- **programs:** fix modal token logic and capitalization/autocorrect settings ([bde2bfa7](/bde2bfa7c0fefcfa677b0b8a8320d86a8fe17d8f))
- **drawer:** fix the foothillranch url ([05b1025b](/05b1025b5d09dda1ee588d93c51a29c1ef78b3fc))
- **gestures:** ensure gesture handler is registered early in the app ([441c5981](/441c5981804f1b19541f5bc1a1d7a24379f369cb))
- **register:** Ensure we pass firstName and lastName to API ([b0c9585a](/b0c9585ab95e1e8b6b9ff02ff59effa55bab61bc))

### Building system
- **beta:** updates beta release process to independently tag and version Android and iOS based on commits. ([c3e5b4fc](/c3e5b4fce650d4e04c41fc43f06a2acd971cb3f3))
- **images:** optimize image sizes ([5b341140](/5b3411404094332d6991eb4e883a457f31cf6ef4))
- **ci:** set git config name and email on CI ([12bedbb3](/12bedbb394d608c892e409f489bccbe5a80569af))
- **changelog:** Update Android CHANGELOG [skip ci] ([3629cd76](/3629cd760c7e6dd82a5fe1798e6b58cb2c2dcd5a))
- **ios:** manually upload testflight ([1ee3ffab](/1ee3ffab9adc645620a4708a168706a601811740))

### Other work
- add forgot password webview ([38997671](/38997671490b61bdf2dd0160d9b6f52244702391))
- add disabled logic to login button ([7e37b298](/7e37b2980e5349d2828e28d7e17c5c06f9fa1355))
- PASSWORD_RESET_URL to env ([4aceaa96](/4aceaa969765fee7b6b7f0fdd2c05f64d44f6da0))

# 1.5.0 iOS Beta (2020-02-20)

### Features:

- **images**: adds fallback images for missing api assets (/cc62c5f984040a84347c7b76ef91dedd5ab05672)
- **Navigation**: Updates to the Custom Drawer (/cc142af15f4922a1162c44c5a78c59861b997beb)
- **release**: Allow beta lane to handle versioning on CI (/b81dc2e63406740a2ea839f9ca48f039ae52aafe)
- **changelog**: generate changelog for ios before taggging (/c9ce6e59771a3d678ad382e0f501057e328f6eab)
- **ios**: sets ITSAppUsesNonExemptEncryption (/8a750d6c81f07f691164691074d5feb476839991)

Bug fixes:

- **release**: fix errors in beta fastlane lane (/2279762d502b65d3bbc7310060b80e72cb23787b)
- **release**: properly match any ios beta tag (/0d28b9e839806fefef2444802bac2436bc80cd7f)
- **release**: properly reference build_number in beta lane (/0012dfcfdef66ac6cd6bdcca62d41319065e4504)

Building system:

- **release**: update slack message (/b0fb93b394e6ea2b95b38bfce7639d5de1cec5ad)

Documentation:

- **changelog**: add changelog (/6c1859fa3b87fa1cb4e033dae7179a5bab117ed3)

Other work:

- add forgot password webview (/38997671490b61bdf2dd0160d9b6f52244702391)
- add disabled logic to login button (/7e37b2980e5349d2828e28d7e17c5c06f9fa1355)
- PASSWORD_RESET_URL to env (/4aceaa969765fee7b6b7f0fdd2c05f64d44f6da0)
- add semantic_release fastlane plugin (/6983a149f7775c36b83386a46f7deadfec005275)
- build(beta)release ios beta 1.1.2 [skip ci](/629550b1dedba746d40d9e04fca133cd285a6d56)

# 1.1.2 iOS Beta (2020-02-20)

### Features

- **release:** Allow beta lane to handle versioning on CI (</7adcfbf97d41d36358dc790a1384f3255b30e207|7adcfbf9>)

### Bug fixes

- **release:** fix errors in beta fastlane lane (</ebece56aff93f07a10e7eb2de9acf60d62d856db|ebece56a>)
- **release:** properly match any ios beta tag (</0680ba0b47fbaa4c9e4ca03eccdbf419f1257aac|0680ba0b>)

### Building system

- **release:** update slack message (</8a8862b0829d101092b703cacde948a9d28fc959|8a8862b0>)

### Other work

- add semantic_release fastlane plugin (</ce08cbc500b65dc5fa1e2d0a1fe7157f3431db15|ce08cbc5>)
