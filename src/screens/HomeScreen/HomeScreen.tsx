import React, { FC, ReactElement } from 'react';
import { ActivityIndicator, RefreshControl } from 'react-native';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import { ContentCard } from '../../components/Card';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';

import { Post } from '../../types';
import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import GenericNewsImage from '../../assets/images/generic_news.jpg';

export const POSTS_QUERY = gql`
  query posts {
    posts {
      id
      title
      date
      description
      media {
        fullImage
      }
      mediaId
    }
  }
`;

export const HomeScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const navigateToPostDetailsScreen = (post: Post): void => {
    navigation.navigate('PostDetails', { postId: post.id });
  };

  const {
    error: postsError,
    data: postsData = {},
    loading: postsLoading,
    refetch: refetchPosts,
  } = useQuery(POSTS_QUERY, {
    fetchPolicy: 'cache-and-network',
  });

  const renderPost = (post: Post): ReactElement => {
    const { id, title, description, media } = post;

    return (
      <Container key={id} pb={2}>
        <ContentCard
          id={id}
          heading={title}
          text={description}
          label=""
          imageSrc={media?.fullImage ? media.fullImage : null}
          imageAsset={GenericNewsImage}
          onPress={(): void => navigateToPostDetailsScreen(post)}
        />
      </Container>
    );
  };

  // TODO: gql types
  const renderPosts = (): ReactElement => {
    const posts = postsData ? postsData.posts : [];

    return <Container px={5}>{posts?.length ? posts.map(renderPost) : null}</Container>;
  };

  const renderHomeScreenContent = (): ReactElement => {
    if (postsLoading && !Object.keys(postsData).length) {
      return (
        <Container fill pt={3} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <Container fullWidth centerContent pt={2} pb={3}>
            <Text testID="hoag-mantra" fontWeight="bold" fontSize={5}>
              {t('home.mantra')}
            </Text>
          </Container>
          <Container fill fullWidth centerContent>
            <ActivityIndicator />
          </Container>
        </Container>
      );
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        refreshControl={<RefreshControl refreshing={postsLoading} onRefresh={refetchPosts} />}
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pt={3} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <Container fullWidth centerContent pt={2} pb={3}>
            <Text testID="hoag-mantra" fontWeight="bold" fontSize={5}>
              {t('home.mantra')}
            </Text>
          </Container>
          {renderPosts()}
        </Container>
      </ScrollView>
    );
  };

  if (postsError) {
    // TODO: handle graphql errors
  }

  return (
    <Screen
      testID="home-screen"
      screenHeader
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
    >
      {renderHomeScreenContent()}
    </Screen>
  );
};
