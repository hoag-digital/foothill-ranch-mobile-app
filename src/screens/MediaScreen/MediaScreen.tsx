import React, { FC } from 'react';

import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';

interface Props {
  /** prop info */
  myProp: string;
}

export const MediaScreen: FC<Props> = () => {
  return (
    <Screen screenHeader backgroundColor="orange">
      <Text>Media Screen</Text>
    </Screen>
  );
};
