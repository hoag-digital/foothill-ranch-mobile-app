import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t } from '../../utils';
import ENV from '../../env';

const INJECTED_JAVASCRIPT = `(function() {
  // On the intial physician list and physician details pages
  var headerDiv = document.querySelector('#header');
  var contentDiv = document.querySelector('#content');
  var primaryDiv = document.querySelector('#primary');

  if (headerDiv) {
    header.style.display = 'none';
  }

  if (contentDiv) {
    content.style.paddingTop = '0';
  }

  if (primaryDiv) {
    primary.style.marginTop = '0';
  }

  // On the actual appointment scheduling page
  var headerPanelDiv = document.querySelector('#HeaderPanel');
  var scheduleThisDoctorDiv = document.querySelector('#ScheduleThisDoctor');
  var footerZoneDiv = document.querySelector('#FooterZone');
  var calendarContainerDiv = document.querySelector('.calendar-container');
  var calendarMainDiv = document.querySelector('.calendar-main');

  if (headerPanelDiv) {
    headerPanelDiv.style.display = 'none';
  }

  if (scheduleThisDoctorDiv) {
    scheduleThisDoctorDiv.style.marginTop = '0';
  }

  if (footerZoneDiv) {
    footerZoneDiv.style.display = 'none';
  }

  if (calendarContainerDiv) {
    calendarContainerDiv.style.width = '100%';
  }

  if (calendarMainDiv) {
    calendarMainDiv.style.display = 'block';
  }
})();`;

export const ScheduleFoothillAppointmentScreen: FC<NavigationStackScreenProps> = () => {
  return (
    <Screen
      testID="care-screen"
      modalHeader
      screenTitle={t('care.scheduleAppointment')}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      <WebView
        source={{ uri: ENV.FOOTHILL_APPOINTMENT_URL }}
        containerStyle={StyleSheet.absoluteFillObject}
        injectedJavaScript={INJECTED_JAVASCRIPT}
        startInLoadingState
      />
    </Screen>
  );
};
