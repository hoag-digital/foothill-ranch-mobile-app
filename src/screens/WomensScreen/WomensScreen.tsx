import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t } from '../../utils';
import ENV from '../../env';

export const WomensScreen: FC<NavigationStackScreenProps> = () => {
  return (
    <Screen
      testID="care-screen"
      modalHeader
      screenTitle={t('care.womens')}
      headerColor={colors.violet}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      <WebView
        source={{ uri: ENV.WOMENS_URL }}
        containerStyle={StyleSheet.absoluteFillObject}
        startInLoadingState
      />
    </Screen>
  );
};
