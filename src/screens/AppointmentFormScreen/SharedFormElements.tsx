import React, { FC, useState } from 'react';
import DatePicker from 'react-native-datepicker';
import ModalSelector from 'react-native-modal-selector';
import dayjs from 'dayjs';
import faker from 'faker';
import { Container } from '../../components/Container/Container';
import { FormFieldError } from '../../components/FormFieldError';
import { TextInput } from '../../components/TextInput/TextInput';
import { Text } from '../../components/Text/Text';
import { colors, radii, space } from '../../styles';
import { t } from '../../utils';

interface Props {
  values: Record<string, any>;
  errors: Record<string, any>;
  handleChange: Function;
  setFieldValue: Function;
}

export const SharedFormElements: FC<Props> = (props: Props) => {
  const { values, errors, handleChange, setFieldValue } = props;

  const today = dayjs();
  const dateFormat = 'M/D/YYYY';
  const timeFormat = 'h:mm A';
  const datePickerPlaceholder = `${t('care.today')} - ${today.format(dateFormat)}`;
  const timePickerPlaceholder = '2:15 PM';

  // State
  const patientTypePlaceholder = t('care.patientType');
  const [patientType, setPatientType] = useState(null);
  const [patientTypeVisible, setPatientTypeVisible] = useState(false);

  // Handlers
  const onPatientTypePickerPress = (): void => setPatientTypeVisible(true);
  const onPatientTypePickerModalClose = (): void => setPatientTypeVisible(false);

  // Module customizations
  const datePickerStyle = { width: '100%' };

  const datePickerCustomStyles = {
    dateInput: {
      color: colors.gainsboro,
      borderColor: colors.gainsboro,
      borderWidth: 1,
      borderRadius: radii[1],
      alignItems: 'flex-start',
      paddingHorizontal: space[4],
    },
  };

  // Mock data
  const MOCK_PATIENT_TYPES: Array<object> = [];

  for (let i = 0; i < 5; i++) {
    const mockPatientType: object = {
      key: i,
      label: faker.lorem.words(3),
    };

    MOCK_PATIENT_TYPES.push(mockPatientType);
  }

  return (
    <>
      <Container
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        maxWidth="100%"
      >
        <Container fill flexBasis="30%" mb={2} mr={2}>
          <DatePicker
            cancelBtnText={t('care.cancel')}
            confirmBtnText={t('care.confirm')}
            customStyles={datePickerCustomStyles}
            date={values.date}
            format={dateFormat}
            mode="date"
            onDateChange={handleChange('date')}
            placeholder={datePickerPlaceholder}
            showIcon={false}
            style={datePickerStyle}
          />
        </Container>
        <Container fill mb={2}>
          <DatePicker
            cancelBtnText={t('care.cancel')}
            confirmBtnText={t('care.confirm')}
            customStyles={datePickerCustomStyles}
            date={values.time}
            format={timeFormat}
            mode="time"
            onDateChange={handleChange('time')}
            placeholder={timePickerPlaceholder}
            showIcon={false}
            style={datePickerStyle}
          />
        </Container>
      </Container>
      {errors.date || errors.time ? (
        <Container>
          {errors.date ? <FormFieldError px={4}>{errors.date}</FormFieldError> : null}
          {errors.time ? <FormFieldError px={4}>{errors.time}</FormFieldError> : null}
        </Container>
      ) : null}
      <Container mb={1}>
        <ModalSelector
          accessible
          data={MOCK_PATIENT_TYPES}
          initValue={t('care.patientType')}
          scrollViewAccessibilityLabel={t('care.scrollableOptions')}
          cancelButtonAccessibilityLabel={t('care.cancelButton')}
          customSelector={
            <Container borderRadius={1} borderColor={colors.gainsboro} borderWidth={1} p={2} px={4}>
              <Text
                color={patientType ? colors.black : colors.silver}
                fontSize={2}
                onPress={onPatientTypePickerPress}
              >
                {patientType ? patientType : patientTypePlaceholder}
              </Text>
            </Container>
          }
          onChange={({ label }): void => {
            // For label display
            setPatientType(label);
            // For actual form value
            setFieldValue('patientType', label);
          }}
          onModalClose={onPatientTypePickerModalClose}
          visible={patientTypeVisible}
        />
        {errors.patientType ? <FormFieldError px={4}>{errors.patientType}</FormFieldError> : null}
      </Container>
      <Container>
        <TextInput
          borderRadius={1}
          borderColor={colors.gainsboro}
          height={100}
          multiline
          placeholder={t('care.addSymptomDetails')}
          pt={3}
          onChangeText={handleChange('symptomDetails')}
          textAlignVertical="top"
          value={values.symptomDetails}
        />
        {errors.symptomDetails ? (
          <FormFieldError px={4}>{errors.symptomDetails}</FormFieldError>
        ) : null}
      </Container>
    </>
  );
};

export default SharedFormElements;
