import { createStackNavigator } from 'react-navigation-stack';

import { CareScreen } from '../screens/CareScreen';
import { AppointmentFormScreen } from '../screens/AppointmentFormScreen/AppointmentFormScreen';
import { ScheduleFoothillAppointmentScreen } from '../screens/ScheduleFoothillAppointmentScreen';
import { WomensScreen } from '../screens/WomensScreen';

/**
 * CareNav: used for navigation pertaining to the "Care" tab
 */
export const CareNav = createStackNavigator(
  {
    Default: {
      screen: CareScreen,
      navigationOptions: {
        header: null,
      },
    },
    'Appointment Form': {
      screen: AppointmentFormScreen,
      navigationOptions: {
        header: null,
      },
    },
    'Schedule Foothill Appointment': {
      screen: ScheduleFoothillAppointmentScreen,
      navigationOptions: {
        header: null,
      },
    },
    Womens: {
      screen: WomensScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Default',
  }
);
