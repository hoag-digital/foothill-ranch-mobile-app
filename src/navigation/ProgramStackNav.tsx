import { createStackNavigator } from 'react-navigation-stack';

import { ProgramIntroScreen } from '../screens/ProgramIntroScreen';
import { ProgramListScreen } from '../screens/ProgramListScreen';
import { ProgramDetailsScreen } from '../screens/ProgramDetailsScreen';

/**
 * ProgramStackNav is the StackNavigator available from the "Programs" tab of the TabBar.
 * Programs are a specific subset of classes which require an Rx token, including Biocircuit and Nutrition.
 * Note that "classes" is the user-facing terminology, and that they correspond to "events" in the codebase.
 */
export const ProgramStackNav = createStackNavigator(
  {
    ProgramList: {
      screen: ProgramListScreen,
    },
    ProgramIntro: {
      screen: ProgramIntroScreen,
    },
    ProgramDetails: {
      screen: ProgramDetailsScreen,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'ProgramList',
  }
);
