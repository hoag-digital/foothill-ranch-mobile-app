import { createStackNavigator } from 'react-navigation-stack';

import { HomeScreen } from '../screens/HomeScreen';
import { PostDetailsScreen } from '../screens/PostDetailsScreen';

/**
 * HomeStackNav is the nav stack available from the Home tab of the TabNav
 */
export const HomeStackNav = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    PostDetails: {
      screen: PostDetailsScreen,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
  }
);
