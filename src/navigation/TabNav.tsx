import React, { ReactElement } from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { colors } from '../styles';
import { DEFAULT_ICON_SIZE } from '../utils/constants';
import { AnimatedTabBar } from '../components/AnimatedTabBar';

import HomeIcon from '../assets/images/menu/home.svg';
import ClassIcon from '../assets/images/menu/classes.svg';
import CareIcon from '../assets/images/menu/care.svg';
import ProgramIcon from '../assets/images/menu/programs.svg';
import ScheduleIcon from '../assets/images/menu/my-classes.svg';
import { HomeStackNav } from './HomeStackNav';
import { ClassStackNav } from './ClassStackNav';
import { CareNav } from './CareNav';
import { ProgramStackNav } from './ProgramStackNav';
import { ScheduleStackNav } from './ScheduleStackNav';

/**
 * Main Nav is main interface of the app, defaults to tabs.
 */
export const TabNav = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStackNav,
      navigationOptions: {
        tabBarTestID: 'home-screen',
        tabBarIcon: (): ReactElement => (
          <HomeIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    ClassCategories: {
      screen: ClassStackNav,
      navigationOptions: {
        tabBarTestID: 'class-categories-screen',
        tabBarIcon: (): ReactElement => (
          <ClassIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    Programs: {
      screen: ProgramStackNav,
      navigationOptions: {
        tabBarTestID: 'program-screen',
        tabBarIcon: (): ReactElement => (
          <ProgramIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    Care: {
      screen: CareNav,
      navigationOptions: {
        tabBarTestID: 'care-screen',
        tabBarIcon: (): ReactElement => (
          <CareIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    Schedule: {
      screen: ScheduleStackNav,
      navigationOptions: {
        tabBarTestID: 'my-schedule-screen',
        tabBarIcon: (): ReactElement => (
          <ScheduleIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    lazy: true,
    tabBarOptions: {
      activeTintColor: colors.black,
      inactiveTintColor: colors.black,
      showLabel: false,
    },
    tabBarComponent: props => {
      return <AnimatedTabBar {...props} />;
    },
  }
);
