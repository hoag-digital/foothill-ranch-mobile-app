import care from './care';
import classes from './classes';
import errors from './errors';
import filter from './filter';
import home from './home';
import login from './login';
import navigation from './navigation';
import programs from './programs';
import registration from './registration';
import schedule from './schedule';

export default {
  care,
  classes,
  errors,
  filter,
  home,
  login,
  navigation,
  programs,
  registration,
  schedule,
};
