export default {
  home: 'Home',
  care: 'Care',
  classes: 'Classes',
  programs: 'Programs',
  schedule: 'Schedule',
};
