export default {
  label: 'Filter',
  all: 'All',
  classes: 'Classes',
  news: 'News',
  videos: 'Videos',
};
