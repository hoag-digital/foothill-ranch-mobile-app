/**
 * This file contains mock data until a functional api is developed.
 * Data in this file is representative of the schema as outlined in mockTypes.ts
 */
import Chance from 'chance';

import { ClassGroup } from '../types';

const chance = new Chance();

export const MOCK_CLASS_IMAGE_1 =
  'https://www.ypchicago.org/wp-content/uploads/2019/04/action-adult-adventure-1058958.jpg';

export const MOCK_CLASS_IMAGE_2 =
  'https://www.sunnysidedental.ca/wp-content/uploads/2019/04/active-blur-body-1574652-1024x683.jpg';

export const MOCK_AVATAR_URI = 'https://i.pravatar.cc/40';
export const MOCK_FACILITY_URI = 'https://placeimg.com/40/40/arch';
const MOCK_DAYS = ['4', '6', '9'];

export const MOCK_CLASS_GROUPS: ClassGroup[] = [
  {
    id: '1',
    title: 'Strength & Flexibility',
    description:
      'Can you reach the top shelf? Do you know your limits? Learn how to use your body more.',
    media: {
      id: '14',
      thumbnailImage: null,
      mediumImage: null,
      fullImage: MOCK_CLASS_IMAGE_1,
    },
  },
  {
    id: '2',
    title: 'Eating Healthy',
    description:
      'Can you reach the top shelf? Do you know your limits? Learn how to use your body more.',
    media: {
      id: '12',
      thumbnailImage: null,
      mediumImage: null,
      fullImage: MOCK_CLASS_IMAGE_1,
    },
  },
  {
    id: '3',
    title: 'Biocircuit',
    description:
      'Can you reach the top shelf? Do you know your limits? Learn how to use your body more.',
    media: {
      id: '11',
      thumbnailImage: null,
      mediumImage: null,
      fullImage: MOCK_CLASS_IMAGE_1,
    },
  },
  {
    id: '4',
    title: 'Nutrition',
    description:
      'Can you reach the top shelf? Do you know your limits? Learn how to use your body more.',
    media: {
      id: '11',
      thumbnailImage: null,
      mediumImage: null,
      fullImage: MOCK_CLASS_IMAGE_1,
    },
  },
];

// TODO - deprecate; only used on ClassDetailsScreen for mock data currently
// needs to be updated in conjunction with the "SuperClass" api enhancement
export const MOCK_CLASSES: any[] = [
  {
    id: 1,
    title: MOCK_CLASS_GROUPS.find(({ id }) => id === '1')?.title || '',
    instructorName: chance.name(),
    instructorAvatarUrl: MOCK_AVATAR_URI,
    startTime: '9am',
    endTime: '11am',
    date: `${chance.month()} ${chance.pickone(MOCK_DAYS)}`,
  },
  {
    id: 2,
    title: MOCK_CLASS_GROUPS.find(({ id }) => id === '2')?.title || '',
    instructorName: chance.name(),
    instructorAvatarUrl: MOCK_AVATAR_URI,
    startTime: '9am',
    endTime: '11am',
    date: `${chance.month()} ${chance.pickone(MOCK_DAYS)}`,
  },
  {
    id: 3,
    title: MOCK_CLASS_GROUPS.find(({ id }) => id === '2')?.title || '',
    instructorName: chance.name(),
    instructorAvatarUrl: MOCK_AVATAR_URI,
    startTime: '9am',
    endTime: '11am',
    date: `${chance.month()} ${chance.pickone(MOCK_DAYS)}`,
  },
];

export const MOCK_URGENT_CARE_APPOINTMENTS = [
  {
    id: chance.guid(),
    facilityName: 'Hoag Urgent Care',
    facilityAvatarUrl: MOCK_FACILITY_URI,
    date: chance.date({ string: true }),
    startTime: '1pm',
  },
  {
    id: chance.guid(),
    facilityName: 'Foothill Urgent Care',
    facilityAvatarUrl: MOCK_FACILITY_URI,
    date: chance.date({ string: true }),
    startTime: '1pm',
  },
];

export const MOCK_URGENT_CARE_LOCATIONS = [
  { id: 1, name: 'Foothill Range', closes: '8pm today', wait: 93, distance: 1 },
  { id: 2, name: 'Anaheim Hills', closes: '8pm today', wait: 25, distance: 2 },
  { id: 3, name: 'Huntington Beach', closes: '8pm today', wait: 78, distance: 3 },
  { id: 4, name: 'Alisa Viejo', closes: '8pm today', wait: 79, distance: 4 },
  { id: 5, name: 'Irvine Sand Canyon', closes: '8pm today', wait: 76, distance: 5 },
  { id: 6, name: 'Foothill Range', closes: '8pm today', wait: 62, distance: 6 },
];
