import { createContext } from 'react';
import { AuthPropType } from '../lib/AuthUtils';

/**
 * Shared context object for authentication.
 */
const AuthContext = createContext<Partial<AuthPropType>>({});

export default AuthContext;
