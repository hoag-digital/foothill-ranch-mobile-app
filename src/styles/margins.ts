export const margins = {
  screenMargin: 20,
};

export const space = [0, 4, 8, 12, 16, 20, 24, 32, 64, 128];
export const radii = [0, 4, 8, 12, 14];
export const radius = 14;
