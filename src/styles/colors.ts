// color names from https://www.htmlcsscolor.com/hex/[HEXVALUE]
const aqua = '#5C8ADB';
const balticSea = '#343A40';
const balticSea50 = 'rgba(52, 58, 64, 0.5)';
const blackTransparent = 'rgba(18, 18, 18, 0.9)';
const grayTransparent = 'rgba(142, 144, 143, 0.7)';
const blueTransparent = 'rgba(77,142,206, 0.10)';
const cabaret = '#D54B68';
const cinnabar = '#E63B3A';
const circleReview = '#083B79';
const cornflowerBlue = '#50A0FF';
const cornflowerBlue2 = '#66A4EE';
const crusta = '#F98549';
const deepBlush = '#EC708A';
const denim = '#2172D5';
const eclipse = '#3f3f3f';
const gainsboro = '#E2E2E2';
const gold = '#EED104';
const gray = '#8E908F';
const lightGreen = '#97EF8E';
const malibu = '#61B2F5';
const mediumOrchid = '#B248FF';
const myChartReview = '#882338';
const pictonBlue = '#4891CE';
const scooter = '#267FA1';
const silver = '#C4C4C4';
const sky = '#E9F1FB';
const skyBlue = '#63B1F5';
const snow = '#FAFAFA';
const solitude = '#DFE4ED';
const solitudeLight = '#ECEFF4';
const summerSky = '#1FC2E0';
const sunsetOrange = '#F04B4B';
const tangerineYellow = '#F0CE07';
const terra = '#E0601F';
const transparent = 'rgba(0, 0, 0, 0)';
const turquoiseBlue = '#5CCCDB';
const viking = '#41ADCA';
const violet = '#E87DDD';
const wellDReview = '#0F4250';
const wellDScooter = '#2D8AA3';
const whisper = '#E4E4E4';
const white = '#fff';
const whiteSmoke = '#F0F0F0';
const whiteSmokeLight = '#F8F8F8';
const whiteTransparent = 'rgba(255,255,255, 0.97)';

export const colors = {
  aqua,
  black: balticSea,
  denim,
  gray,
  terra,
  balticSea,
  balticSea50,
  blackTransparent,
  blueTransparent,
  cinnabar,
  circleReview,
  cornflowerBlue,
  cornflowerBlue2,
  crusta,
  eclipse,
  gainsboro,
  gold,
  grayTransparent,
  lightGreen,
  malibu,
  mediumOrchid,
  myChartReview,
  pictonBlue,
  scooter,
  silver,
  sky,
  skyBlue,
  snow,
  solitude,
  summerSky,
  sunsetOrange,
  tangerineYellow,
  transparent,
  turquoiseBlue,
  violet,
  wellDReview,
  whisper,
  white,
  whiteTransparent,
  whiteSmoke,
  whiteSmokeLight,
  classes: mediumOrchid,
  disabled: silver,
  grayText: eclipse,
  news: malibu,
  primary: scooter,
};

export const gradients = {
  circle: [cornflowerBlue, denim],
  mychart: [deepBlush, cabaret],
  wellD: [viking, wellDScooter],
  solitude: [solitudeLight, solitude],
  whiteSolitudeLight: [white, solitudeLight],
  blackTransparentClear: [blackTransparent, transparent],
};
