import decodeHtml from './decodeHtml';

describe('decode html', () => {
  test('returns string with special characters decoded', () => {
    expect(decodeHtml(`Program Patch Coffee &#8211; The Perfect Blend`)).toBe(
      'Program Patch Coffee – The Perfect Blend'
    );
  });

  test('returns empty string when value is null', () => {
    expect(decodeHtml(null)).toBe('');
  });
  
  test('returns empty string when value is undefined', () => {
    expect(decodeHtml(undefined)).toBe('');
  });
});
