import { Linking } from 'react-native';

export const safelyOpenUrl = async (url?: string | null): Promise<any> => {
  if (!url) return null;

  const supported = await Linking.canOpenURL(url);

  if (supported) {
    return Linking.openURL(url);
  }

  return null;
};

export default safelyOpenUrl;
