import { allFieldsHaveValues } from './form'

describe('no values', () => {
  test('returns something expected', () => {
    const fields = [
      'email',
      'password'
    ];

    const values = {
    }

    const result = allFieldsHaveValues(fields, values);

    expect(result).toBeFalsy();
  });
});