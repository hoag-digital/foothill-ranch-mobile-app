import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { Text } from '../Text';
import { CustomDrawerItem } from './CustomDrawerItem';

storiesOf('components/CustomDrawerItem', module).add('Default', () => (
  <CustomDrawerItem itemKey="Main">
    <Text>Main</Text>
  </CustomDrawerItem>
));
