import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { Datepicker } from './Datepicker';

storiesOf('components/Datepicker', module).add('Default', () => <Datepicker />);
