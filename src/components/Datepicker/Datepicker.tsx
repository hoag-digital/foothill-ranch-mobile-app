import React, { FC } from 'react';
import { Icon } from 'react-native-elements';
import CalendarPicker from 'react-native-calendar-picker';
import dayjs from 'dayjs';

import { colors, space } from '../../styles';
import { Container } from '../Container';
import { Modal } from '../Modal';
import { Text } from '../Text';
import { Touchable } from '../Touchable';

/**
 * Component docs: Date picker icon that will bring a modal up from bottom of the screen with a datepicker
 *  NOTE: currently doesn't fully meet the design, limitation of the library that does not allow different color text for range dates
 */
export const Datepicker: FC = () => {
  const [isVisible, setIsVisible] = React.useState(false);
  const minDate = new Date();
  const startEnd = dayjs(minDate).add(4, 'days');
  const [selectedStartDate, setStartDate] = React.useState(minDate);
  const [selectedEndDate, setEndDate] = React.useState(startEnd);

  const startRangeStyle = {
    backgroundColor: colors.black,

    borderRadius: 25,
    height: 30,
    width: 30,
  };

  const endRangeStyle = {
    backgroundColor: colors.terra,
    borderRadius: 25,
    height: 30,
    width: 30,
  };

  const selecteStyle = {
    backgroundColor: colors.whisper,
    height: 20,
    width: 75,
  };

  const linkStyle = { paddingRight: space[3] };

  const selectDates = (): void => {
    setIsVisible(false);
    //TODO: use the values
    console.log('startDate', dayjs(selectedStartDate).format('MM/DD/YYYY'));
    console.log('endDate', dayjs(selectedEndDate).format('MM/DD/YYYY'));
  };

  return (
    <>
      <Touchable
        onPress={(): void => {
          setIsVisible(true);
        }}
      >
        <Container flexDirection="row" alignItems="center">
          <Icon name="date-range" />
          <Text fontSize={1}>Date</Text>
        </Container>
      </Touchable>
      <Modal isVisible={isVisible} setIsVisible={setIsVisible} bottomHalf>
        <Container fullWidth centerContent>
          <Text>Calendar</Text>
          <CalendarPicker
            minDate={minDate}
            allowRangeSelection
            todayBackgroundColor={colors.transparent}
            todayTextStyle={{ color: colors.black }}
            selectedDayTextColor={colors.white}
            selectedStartDate={selectedStartDate}
            selectedEndDate={selectedEndDate}
            selectedRangeStartStyle={startRangeStyle}
            selectedRangeEndStyle={endRangeStyle}
            selectedRangeStyle={selecteStyle}
            onDateChange={(date, type): void => {
              console.log(date, type);

              if (type === 'END_DATE') {
                setEndDate(date);
              } else {
                setStartDate(date);
              }
            }}
          />
          <Container flexDirection="row" fullWidth justifyContent="flex-end">
            <Touchable onPress={(): void => setIsVisible(false)} {...linkStyle}>
              <Text>Cancel</Text>
            </Touchable>
            <Touchable onPress={(): void => selectDates()} {...linkStyle}>
              <Text>Ok</Text>
            </Touchable>
          </Container>
        </Container>
      </Modal>
    </>
  );
};

export default Datepicker;
