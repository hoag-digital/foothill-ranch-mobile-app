import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { Screen } from '../Screen';
import { Login } from './Login';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const ScreenDecorator = storyFn => <Screen padding={10}>{storyFn()}</Screen>;

storiesOf('components/Login', module)
  .addDecorator(ScreenDecorator)
  .add('Default', () => (
    <Login
      loginPress={(): void => console.log('login pressed')}
      forgotPasswordPress={(): void => console.log('forgot password pressed')}
      registrationPress={(): void => console.log('registration pressed')}
    />
  ));
