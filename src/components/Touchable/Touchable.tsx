/* eslint-disable @typescript-eslint/explicit-function-return-type */ //This is to resolve lint error on the props functions
import styled from '@emotion/native';
import {
  BorderProps,
  borders,
  color,
  ColorProps,
  flexbox,
  FlexProps,
  layout,
  position,
  PositionProps,
  space,
  SpaceProps,
} from 'styled-system';

interface TouchableProps {
  /** applies "flex: 1" style **/
  fill?: boolean;
  /** applies "width: 100%" style **/
  fullWidth?: boolean;
  /** centers content both vertically and horizontally **/
  centerContent?: boolean;
}

type ComponentProps = TouchableProps &
  BorderProps &
  ColorProps &
  FlexProps &
  SpaceProps &
  PositionProps;

/**
 * This is our primitive TouchableOpacity component with styled-system props applied
 */
export const Touchable = styled.TouchableOpacity<ComponentProps>`
  ${space};
  ${color};
  ${borders};
  ${position}
  ${layout};
  ${flexbox};

  ${props =>
    props.fill &&
    `
      flex: 1;
    `}

  ${props =>
    props.fullWidth &&
    `
      width: 100%;
    `}

  ${props =>
    props.centerContent &&
    `
      justifyContent: center;
      alignItems: center;
    `}
`;

Touchable.defaultProps = {};
