import React, { FC } from 'react';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { Keyboard } from 'react-native';
import { Button } from '../Button';
import { Modal } from '../Modal';
import { Text } from '../Text';
import { colors } from '../../styles';
import { Container } from '../Container';
import { SCREEN_WIDTH, t } from '../../utils';

interface Props {
  /** Whether or not the Modal is visible */
  isVisible: boolean;
  /** The function for setting the visibility */
  setIsVisible: (visible: boolean) => void;
  /**  The function that will be used when clicking validate */
  validate: () => void;
  /** The function that will be used to set the userToken  */
  setTokenValue: (code: string) => void;
  /** current userToken value */
  userTokenValue: string;
  /** does token have an error, if so display error message */
  tokenError: boolean;
}

/**
 * This is the modal used to allow a user to enter an Rx (Program Authorization) token
 */
export const RxTokenModal: FC<Props> = ({
  isVisible,
  setIsVisible,
  validate,
  setTokenValue,
  userTokenValue,
  tokenError,
}) => {
  return (
    <Modal isVisible={isVisible} setIsVisible={setIsVisible} displayClose={false}>
      <Container mt={10} mb={1} alignItems="center">
        <Text fontSize={3} my={2} textAlign="center">
          {t('programs.token.modal.text')}
        </Text>
        <SmoothPinCodeInput
          codeLength={6}
          cellStyle={{
            borderBottomWidth: 2,
            borderColor: 'gray',
          }}
          cellStyleFocused={{
            borderColor: 'black',
          }}
          textStyle={{ fontSize: 35 }}
          containerStyle={{ marginVertical: 15 }}
          keyboardType="default"
          inputProps={{
            autoCapitalize: 'none',
            autoCorrect: false,
          }}
          value={userTokenValue}
          onTextChange={(code): void => setTokenValue(code)}
          onFulfill={(): void => Keyboard.dismiss()}
        />

        {/** changing text from transparent to red so the screen does not jump if there is an error */}
        <Text fontSize={3} color={tokenError ? 'red' : colors.transparent}>
          {t('programs.token.modal.error')}
        </Text>
        <Button
          py={3}
          label={t('programs.token.modal.button')}
          color={colors.white}
          borderRadius={5}
          mt={5}
          mb={20}
          width={SCREEN_WIDTH * 0.6}
          onPress={(): void => {
            validate();
          }}
        />
      </Container>
    </Modal>
  );
};

export default RxTokenModal;
