import React, { FC } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { NavigationSwitchScreenProps, withNavigation } from 'react-navigation';
import { FlexProps, SpaceProps } from 'styled-system';

import { Icon } from 'react-native-elements';

import Logo from '../../assets/images/hoag-logo-solid.svg';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';
import { LARGER_ICON_SIZE, STATUS_BAR_PADDING_SCALE, t, DEFAULT_HITSLOP } from '../../utils';
import { LoggedOutContent } from '../../lib/AuthUtils';

interface ScreenHeaderProps extends NavigationSwitchScreenProps {
  /** Whether to display the drawer menu button */
  drawerEnabled?: boolean;
}

type ComponentProps = ScreenHeaderProps & FlexProps & SpaceProps;

const ScreenHeaderComponent: FC<ComponentProps> = ({ drawerEnabled, navigation }) => {
  const toggleDrawer = (): void => {
    navigation.toggleDrawer();
  };

  const navigateToLogin = (): void => {
    navigation.navigate('Login');
  };

  return (
    <Container bg={colors.white}>
      <Container
        minHeight={30}
        flexDirection="row"
        justifyContent="space-around"
        alignItems="center"
        fullWidth
        borderBottomWidth={1}
        borderBottomColor={colors.whisper}
        pt={STATUS_BAR_PADDING_SCALE}
        pb={2}
      >
        <Container width="30%" flexDirection="row" justifyContent="flex-start">
          {drawerEnabled && (
            <Touchable testID="drawer-icon" onPress={toggleDrawer} px={3} hitSlop={DEFAULT_HITSLOP}>
              <Icon name="ios-menu" type="ionicon" size={LARGER_ICON_SIZE} color={colors.black} />
            </Touchable>
          )}
        </Container>
        <Container width="30%" centerContent flexDirection="row">
          <Logo />
          <Text color={colors.gray} fontSize={2} marginTop={2}>
            {` | ${t('home.name')}`}
          </Text>
        </Container>
        <Container width="30%" />
      </Container>
      {/* Global login banner for all screens leveraging this component */}
      <LoggedOutContent>
        <TouchableWithoutFeedback onPress={navigateToLogin}>
          <Container py={1} centerContent bg={colors.terra}>
            {/* TODO: i18n */}
            <Text fontSize={1} color={colors.white}>
              Login to gain full access to the app!{' '}
              <Text fontWeight="bold" fontSize={1} color={colors.white}>
                →
              </Text>
            </Text>
          </Container>
        </TouchableWithoutFeedback>
      </LoggedOutContent>
      {/* end Login banner */}
    </Container>
  );
};

ScreenHeaderComponent.defaultProps = {
  drawerEnabled: false,
};

export const ScreenHeader = withNavigation(ScreenHeaderComponent);
