import React, { FC } from 'react';
import { ActivityIndicator } from 'react-native';
import {
  backgroundColor,
  BorderProps,
  ColorProps,
  SpaceProps,
  FlexProps,
  LayoutProps,
} from 'styled-system';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';

interface ButtonProps {
  /** accessibility label */
  accessibilityLabel: string;
  /** disabled button state */
  disabled?: boolean;
  /** loading button state */
  loading?: boolean;
  /** the text label of the button */
  label: string;
  /** the callback to be invoked onPress */
  onPress: () => void;
  renderIcon?: Function;
  /** whether to ignore the default disabled color variation */
  ignoreDisabledStyles: boolean;
}

type ComponentProps = ButtonProps & BorderProps & ColorProps & SpaceProps & FlexProps & LayoutProps;

/**
 * notes:
 * - restricting inner text style from being directly configurable to avoid style prop conflicts
 * - if button is disabled it will not render a touchableOpacity at all
 */
export const Button: FC<ComponentProps> = ({
  accessibilityLabel,
  renderIcon,
  label,
  onPress,
  disabled,
  loading,
  color: componentColor,
  borderColor,
  ignoreDisabledStyles,
  ...props
}) => {
  const ButtonContainer = disabled ? Container : Touchable;
  const onPressAction = loading ? null : onPress;
  const defaultTextColor = disabled ? colors.silver : componentColor;
  const textColor = ignoreDisabledStyles ? componentColor : defaultTextColor;

  return (
    <ButtonContainer
      centerContent
      bg={disabled ? colors.disabled : backgroundColor}
      borderColor={disabled ? colors.disabled : borderColor}
      borderRadius={40}
      accessibilityLabel={accessibilityLabel}
      flexDirection="row"
      onPress={onPressAction}
      disabled={disabled}
      {...props}
    >
      {loading ? (
        <ActivityIndicator />
      ) : (
        <>
          {renderIcon ? renderIcon() : null}

          <Text color={textColor} fontSize={2}>
            {label}
          </Text>
        </>
      )}
    </ButtonContainer>
  );
};

Button.defaultProps = {
  disabled: false,
  borderColor: colors.transparent,
  borderWidth: 1,
  backgroundColor: colors.terra,
  loading: false,
  icon: null,
};
