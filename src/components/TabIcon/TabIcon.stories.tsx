import { storiesOf } from '@storybook/react-native';
import React from 'react';

import HomeIcon from '../../assets/images/menu/home.svg';
import ClassIcon from '../../assets/images/menu/classes.svg';
import CareIcon from '../../assets/images/menu/urgent-care.svg';
import MediaIcon from '../../assets/images/menu/media.svg';
import ScheduleIcon from '../../assets/images/menu/my-classes.svg';

import { Container } from '../Container';
import { TabIcon } from './TabIcon';

storiesOf('components/TabIcon', module).add('Nav', () => (
  <Container flexDirection="row">
    <TabIcon displayBadge={false} label="Home" icon={<HomeIcon height={30} width={30} />} />
    <TabIcon displayBadge={false} label="Classes" icon={<ClassIcon height={30} width={30} />} />
    <TabIcon displayBadge={false} label="Care" icon={<CareIcon height={30} width={30} />} />
    <TabIcon displayBadge={false} label="Media" icon={<MediaIcon height={30} width={30} />} />
    <TabIcon displayBadge={false} label="Schedule" icon={<ScheduleIcon height={30} width={30} />} />
  </Container>
));
