import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { ClassesCategoryLabel } from './ClassesCategoryLabel';

storiesOf('components/ClassesCategoryLabel', module).add('Default', () => <ClassesCategoryLabel />);
