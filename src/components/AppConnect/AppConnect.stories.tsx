import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { Screen } from '../Screen';
import { colors, gradients } from '../../styles';
import myChart from '../../assets/images/myChartLogo.png';
import circleLogo from '../../assets/images/circleLogo.png';
import wellDLogo from '../../assets/images/wellDlogo.png';
import { AppConnect } from './AppConnect';

storiesOf('components/Appconnect', module).add('Default', () => (
  <Screen color={colors.transparent}>
    <AppConnect
      app="My Chart"
      subtitle="Epic"
      appReview="#3 in Medical"
      logo={myChart}
      gradient={gradients.mychart}
      reviewColor={colors.myChartReview}
    />
    <AppConnect
      app="Circle"
      subtitle="Circle Medical Tech"
      appReview="200 reviews"
      logo={circleLogo}
      gradient={gradients.circle}
      reviewColor={colors.circleReview}
    />
    <AppConnect
      app="WellD"
      subtitle="Epic"
      appReview="135 reviews"
      logo={wellDLogo}
      gradient={gradients.wellD}
      reviewColor={colors.wellDReview}
    />
  </Screen>
));
