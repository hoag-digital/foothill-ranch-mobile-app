import React, { FC } from 'react';
import styled from '@emotion/native';
import { Container } from '../Container';
import { Text } from '../Text';
import { colors } from '../../styles';

interface Props {
  children: React.ReactNode;
}

// This allows us to use styled-system props when instantiating the component.
const StyledContainer = styled(Container)``;

/**
 * UI for error text that displays for a single form field.
 */
export const FormFieldError: FC<Props> = props => {
  const { children } = props;

  return (
    <StyledContainer {...props}>
      <Text color={colors.terra}>{children}</Text>
    </StyledContainer>
  );
};

export default FormFieldError;
