import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { AnimatedTabBar } from './AnimatedTabBar';

storiesOf('components/AnimatedTabBar', module).add('Default', () => <AnimatedTabBar />);
