import React, { FC } from 'react';
import dayjs from 'dayjs';
import { Button } from '../Button';
import { Modal } from '../Modal';
import { Text } from '../Text';
import { colors } from '../../styles';
import { Container } from '../Container';
import { SCREEN_WIDTH, t } from '../../utils';

interface Props {
  /** Whether or not the Modal is visible */
  isVisible: boolean;
  /** The function for setting the visibility */
  setIsVisible: (visible: boolean) => void;
  /**  The date the token will expire */
  tokenExpiration: Date;
}

/**
 * Component docs: Component of type Modal that displays to prompt user to login when attempting perform certain actions or view some content
 */
export const TokenInvalidModal: FC<Props> = ({ isVisible, setIsVisible, tokenExpiration }) => {
  return (
    <Modal isVisible={isVisible} setIsVisible={setIsVisible} displayClose={false}>
      <Container mt={10} mb={1} alignItems="center">
        <Text fontSize={3} my={2} textAlign="center">
          {`${t('programs.token.modal.expired.expiration')} ${dayjs(tokenExpiration).format(
            'dddd, MMMM DD'
          )}`}
        </Text>

        <Button
          py={3}
          label={t('programs.token.modal.expired.button')}
          color={colors.white}
          borderRadius={5}
          mt={5}
          mb={20}
          width={SCREEN_WIDTH * 0.6}
          onPress={(): void => {
            setIsVisible(false);
          }}
        />
      </Container>
    </Modal>
  );
};

export default TokenInvalidModal;
