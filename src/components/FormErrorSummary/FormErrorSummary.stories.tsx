import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { FormErrorSummary } from './FormErrorSummary';

storiesOf('components/FormErrorSummary', module).add('Default', () => (
  <FormErrorSummary>The following fields are required: email, name.</FormErrorSummary>
));
