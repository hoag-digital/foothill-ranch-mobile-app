import * as Sentry from '@sentry/react-native';
import React, { FC, ReactElement } from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Linking,
  TouchableHighlight,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { DrawerActions, DrawerItems, DrawerContentComponentProps } from 'react-navigation-drawer';
import { NavigationActions } from 'react-navigation';
import FastImage from 'react-native-fast-image';
import VersionNumber from 'react-native-version-number';

import { Text } from '../Text';
import { Container } from '../Container';
import { space } from '../../styles/margins';
import { colors } from '../../styles/colors';
import { CustomDrawerItem } from '../CustomDrawerItem';
import CloseIcon from '../../assets/images/secondary-nav-x-icon.svg';
import HoagMenuImage from '../../assets/images/hoag_menu_image.jpeg';
import { MENU_IMAGE_SIZE, MENU_WIDTH, IS_ANDROID, DEFAULT_HITSLOP } from '../../utils';
import { safelyOpenUrl } from '../../utils/safelyOpenUrl';
import ENV from '../../env';

const { USE_SENTRY } = ENV;

const { appVersion, buildVersion } = VersionNumber;

interface CustomDrawerNavProps extends DrawerContentComponentProps {
  /** boolean which indicates is the active user is "logged-out" */
  isLoggedOut: boolean;
}

const FacilityName = 'Hoag Foothill Ranch';
const HoagAddress = '26672 Portola Parkway\nFoothill Ranch, CA 92610\n';
const HoagPhoneNumber = '800-400-4624';
const HoagWebsite = 'https://hoagfoothillranch.com';
const phoneNumber = HoagPhoneNumber.replace(/-/g, '');
const phoneUrl = Platform.OS === 'ios' ? `telprompt:${phoneNumber}` : `tel:${phoneNumber}`;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    paddingTop: space[6],
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
});

const TransparentDivider = (): ReactElement => (
  <Container
    borderTopColor={colors.silver}
    borderTopWidth={1}
    marginBottom={space[4]}
    opacity={0.2}
  />
);

/**
 * Custom onItemPress function which overrides the default navigation
 */
export const onItemPress = async (drawerNavProps, itemPressProps, auth): Promise<any> => {
  const isMockTabRoute = itemPressProps.route.key.indexOf('-Tab') > -1;

  // simulates selecting the tab from the tab bar
  if (isMockTabRoute) {
    const tabRoute = itemPressProps.route.key.replace('-Tab', '');

    // returns i.e. 'Home/Care/Default'... we want the middle portion (corresponds to the active tab).
    const navigatorStatePath = drawerNavProps.navigation.router.getPathAndParamsForState(
      drawerNavProps.navigation.state
    ).path;

    const activeRoute = navigatorStatePath.split('/')[1];

    // if the user selects the currently active route, just close the drawer
    if (activeRoute === tabRoute) {
      drawerNavProps.navigation.dispatch(DrawerActions.closeDrawer());

      return;
    }

    // otherwise navigate to the corresponding tab route
    drawerNavProps.navigation.navigate(tabRoute);

    return;
  }

  // allows user to logout directly from the drawer
  if (itemPressProps.route.key === 'Logout') {
    if (auth && auth.token) {
      await auth.clearToken();

      if (USE_SENTRY) {
        Sentry.setUser(null);
      }

      drawerNavProps.navigation.dispatch(DrawerActions.CLOSE_DRAWER);
      drawerNavProps.navigation.reset([NavigationActions.navigate({ routeName: 'Home' })], 0);

      return;
    } else {
      drawerNavProps.navigation.navigate('Login');

      return;
    }
  }

  // falls back to the default behavior
  return drawerNavProps.onItemPress({
    route: itemPressProps.route,
    focused: itemPressProps.focused,
  });
};

/**
 * CustomDrawerNav: this component is used as the draw navigator
 */
export const CustomDrawerNav: FC<CustomDrawerNavProps> = props => {
  const { isLoggedOut } = props;

  return (
    <ScrollView contentContainerStyle={styles.outerContainer}>
      <SafeAreaView style={styles.innerContainer}>
        <Container marginLeft={4} marginTop={IS_ANDROID ? space[6] : 0}>
          <TouchableOpacity
            onPress={(): void => props.navigation.toggleDrawer()}
            hitSlop={DEFAULT_HITSLOP}
          >
            <CloseIcon height={15} width={15} />
          </TouchableOpacity>
        </Container>
        <Container
          testID="drawer-items"
          flex={1}
          flexDirection="column"
          justifyContent="space-between"
          marginTop={6}
          marginLeft={4}
        >
          <DrawerItems
            {...props}
            getLabel={(scene): ReactElement => (
              <CustomDrawerItem isLoggedOut={isLoggedOut} itemKey={scene.route.key}>
                {props.getLabel(scene)}
              </CustomDrawerItem>
            )}
          />
        </Container>
        <Container
          testID="hoag-contact"
          flex={1}
          padding={4}
          alignItems="flex-start"
          justifyContent="flex-end"
        >
          <TransparentDivider />
          <Text testID="app-version-number" fontSize={2} lineHeight={space[4]}>
            {`App Version ${appVersion}`}
          </Text>
          <Text testID="app-build-number" fontSize={2} lineHeight={space[4]} pb={3}>
            {`Build ${buildVersion}`}
          </Text>
          <FastImage
            accessibilityLabel="Foothill ranch facility image"
            style={{
              width: MENU_WIDTH,
              height: MENU_IMAGE_SIZE,
            }}
            source={HoagMenuImage}
          />
          <Text testID="hoag-facility-name" fontSize={4} fontWeight="bold" py={2}>
            {FacilityName}
          </Text>
          <Text testID="hoag-address" fontSize={2} lineHeight={space[4]}>
            {HoagAddress}
          </Text>
          <Text
            testID="hoag-phone"
            fontSize={2}
            color={colors.malibu}
            textDecorationLine="underline"
            marginTop={space[3]}
            marginBottom={space[3]}
            onPress={(): Promise<any> => safelyOpenUrl(phoneUrl)}
          >
            {HoagPhoneNumber}
          </Text>

          <TouchableHighlight onPress={(): Promise<any> => Linking.openURL(HoagWebsite)}>
            <Text
              testID="hoag-website"
              fontSize={2}
              color={colors.malibu}
              textDecorationLine="underline"
            >
              hoagfoothillranch.com
            </Text>
          </TouchableHighlight>
        </Container>
      </SafeAreaView>
    </ScrollView>
  );
};

export default CustomDrawerNav;
