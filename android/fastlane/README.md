fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android alpha
```
fastlane android alpha
```
Signed alpha build
### android beta
```
fastlane android beta
```
Deploy a new version to Google Play
### android beta_local
```
fastlane android beta_local
```
This runs tasks typically done on CI before calling the beta lane
### android release
```
fastlane android release
```
Deploy a new version to Google Play
### android release_local
```
fastlane android release_local
```
This runs tasks typically done on CI before calling the release lane

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
