import imagemin from 'imagemin';
import gif from 'imagemin-gifsicle';
import jpg from 'imagemin-jpegoptim';
import png from 'imagemin-optipng';
import svg from 'imagemin-svgo';

const plugins = [
  gif({ interlaced: true }),
  jpg({ progressive: true, max: 90 }),
  png({ optimizationLevel: 5 }),
  svg({ plugins: [{ removeViewBox: false }] }),
];

export async function minifyFile(filename): Promise<void> {
  await imagemin([filename], { destination: `${filename}/..`, use: plugins });
}

async function processFiles(): Promise<any> {
  const files = process.argv.slice(2).map(minifyFile);

  try {
    await Promise.all(files);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

processFiles();
