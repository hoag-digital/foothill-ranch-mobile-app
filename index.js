// Import gesture handler early so that it's registered when navs are created
// See: https://github.com/software-mansion/react-native-gesture-handler/issues/320
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(App));
